import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';


import { AppComponent } from './app.component';
import { ListaParticipantesComponent } from './ListaParticipantes/lista-participantes.component';
import { ResultadoSorteioComponent } from './ResultadoSorteio/resultado-sorteio.component';
import { ListaParticipantesService } from './ListaParticipantes/lista-participantes.service';
import { ResultadoSorteioService } from './ResultadoSorteio/resultado-sorteio.service';
import { CpfMaskPipe } from './shared/CpfPipe';

@NgModule({
  declarations: [
    AppComponent,
    ListaParticipantesComponent,
    ResultadoSorteioComponent,
    CpfMaskPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatTabsModule,
    MatToolbarModule,
    MatSnackBarModule
  ],
  providers: [
    ListaParticipantesService,
    ResultadoSorteioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
