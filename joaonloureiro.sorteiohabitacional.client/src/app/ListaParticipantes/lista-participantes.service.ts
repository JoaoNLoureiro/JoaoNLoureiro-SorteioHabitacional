import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Pessoa } from 'src/app/shared/Pessoa';


@Injectable()
export class ListaParticipantesService {

  resource = 'pessoas';

  constructor(private http: HttpClient) { }

  getPessoas() {
    let params = new HttpParams();
    return this.http.get<Array<Pessoa>>('/pessoa', { params: params, observe: 'response' });
  }

  uploadPessoas(arquivo : FormData) {
    let params = new HttpParams();
    return this.http.post<Array<Pessoa>>('/pessoa', arquivo, { params: params, observe: 'response' });
  }

  deletePessoas() {
    let params = new HttpParams();
    return this.http.delete('/pessoa', { params: params, observe: 'response' });
  }
}
