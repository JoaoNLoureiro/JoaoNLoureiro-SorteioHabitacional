import { Component, OnInit } from '@angular/core';
import { Pessoa } from 'src/app/shared/Pessoa';
import { ListaParticipantesService } from './lista-participantes.service';
import { groupBy } from '../shared/utils';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'lista-participantes',
  templateUrl: './lista-participantes.component.html',
  styleUrls: ['./lista-participantes.component.css']
})
export class ListaParticipantesComponent implements OnInit {
  public pessoas: Array<Pessoa> = [];
  public pessoasPorCota: Record<string, Pessoa[]> | undefined;

  file: File | undefined;

  constructor(private pessoasService: ListaParticipantesService,
    private router: Router,
    private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.pessoasService.getPessoas().subscribe({
      next: (result) => {
        this.pessoas = result.body ?? [];
        this.pessoasPorCota = this.getPessoasPorCota();
      },
      error: (error) => console.log(error)
    });
  }

  onFileChange(event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList | null = element.files;
    if (fileList) {
      this.file = fileList[0];
    }
  }

  onUpload() {
    if (this.file) {
      const formData = new FormData();
      formData.append('file', this.file, this.file.name);

      this.pessoasService.uploadPessoas(formData).subscribe({
        next: (result) => {
          this.pessoas = result.body ?? [];
          this.pessoasPorCota = this.getPessoasPorCota();
        },
        error: (error) => console.log(error)
      });
    }
  }

  onClear () {
    this.pessoasService.deletePessoas().subscribe({
      next: () => {
        this.pessoas = [];
        this.pessoasPorCota = undefined;
      },
      error: (error) => console.log(error)
    });
  }

  getPessoasPorCota() {
    return groupBy(this.pessoas, (p) => p.cota);
  }

  RealizarSorteio() {
    if (this.pessoas.length <= 0) {
      this.openSnackBar('Carregue os participantes antes de realizar o sorteio!', 'Fechar');
      return;
    }

    this.router.navigate(['/resultado']);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      panelClass: ['mat-toolbar', 'mat-warn']
    });
  }
}
