export interface Pessoa {
  nome: string;
  cpf: string;
  dataNascimento: Date | null;
  renda: number;
  cota: string;
  cid: string;
}
