import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cpfMask'
})
export class CpfMaskPipe implements PipeTransform {

  transform(value: string): string {
    if (!value) {
      return '';
    }

    const partesCpf = value.split('.');
    if (partesCpf.length === 3) {
      partesCpf[1] = 'XXX';
      const digito = partesCpf[2].split('-');
      if (digito.length === 2) {
        digito[0] = 'XXX';
        digito[1] = 'X' + digito[1].substring(1);
        partesCpf[2] = digito.join('-');
      }
    }

    return partesCpf.join('.');
  }
}
