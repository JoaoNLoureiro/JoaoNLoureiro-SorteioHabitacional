import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Pessoa } from '../shared/Pessoa';

interface SorteioResponse {
  ganhadoresPorCota: Record<string, Pessoa[]>;
}

@Injectable()
export class ResultadoSorteioService {

  resource = 'sorteio';

  constructor(private http: HttpClient) { }

  getResultadoSorteio() {
    let params = new HttpParams();
    return this.http.get<SorteioResponse>(`${this.resource}`, { params: params, observe: 'response' });
  }
}
