import { Component, OnInit } from '@angular/core';
import { ResultadoSorteioService } from './resultado-sorteio.service';
import { Pessoa } from '../shared/Pessoa';

@Component({
  selector: 'resultado-sorteio',
  templateUrl: './resultado-sorteio.component.html',
  styleUrls: ['./resultado-sorteio.component.css']
})
export class ResultadoSorteioComponent implements OnInit {
  ganhadoresSorteio: Record<string, Pessoa[]> | undefined;

  constructor(private service: ResultadoSorteioService) { }

  ngOnInit(): void {
    this.RealizarSorteio();
  }

  RealizarSorteio() {
    this.service.getResultadoSorteio().subscribe({
      next: (resultado) => this.ganhadoresSorteio = resultado.body?.ganhadoresPorCota ?? undefined,
      error: (error) => console.error(error)
    });
  }
}
