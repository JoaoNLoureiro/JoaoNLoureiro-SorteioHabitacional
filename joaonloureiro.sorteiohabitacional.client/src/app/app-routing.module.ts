import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaParticipantesComponent } from './ListaParticipantes/lista-participantes.component';
import { ResultadoSorteioComponent } from './ResultadoSorteio/resultado-sorteio.component';

const routes: Routes = [
  { path: '', component: ListaParticipantesComponent },
  { path: 'resultado', component: ResultadoSorteioComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
