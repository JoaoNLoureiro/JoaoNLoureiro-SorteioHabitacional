﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.DTOs;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Repositories;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Validators;
using JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Repositories;
using JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Services;
using JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Services.SorteioStrategies;
using JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Validators;

namespace JoaoNLoureiro.SorteioHabitacional.Server.Configuration;

public static class DependencyInjectionConfiguration
{
    public static IServiceCollection AddDependencyInjection(this IServiceCollection services)
    {
        services.AddSingleton<IPessoaRepository, PessoaRepository>();
        services.AddScoped<IPessoaValidator, PessoaValidator>();

        services.AddScoped<ISorteioService, SorteioService>();
        services.AddScoped<ISorteioStrategy, SorteioGeralStrategy>();
        services.AddScoped<ISorteioStrategy, SorteioIdosoStrategy>();
        services.AddScoped<ISorteioStrategy, SorteioDeficienteStrategy>();
        services.AddScoped<IExcelReader, ExcelReader>();

        services.AddTransient<IResultadoSorteio, ResultadoSorteioDto>();

        return services;
    }
}