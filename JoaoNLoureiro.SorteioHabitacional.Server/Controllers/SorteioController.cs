﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace JoaoNLoureiro.SorteioHabitacional.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class SorteioController(ISorteioService service) : ControllerBase
{
    [HttpGet(Name = "GetSorteio")]
    public IActionResult Get()
    {
        return Ok(service.RealizarSorteio());
    }
}