﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Repositories;
using Microsoft.AspNetCore.Mvc;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Validators;

namespace JoaoNLoureiro.SorteioHabitacional.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class PessoaController : ControllerBase
{
    private readonly IPessoaRepository _repository;
    private readonly IPessoaValidator _validator;
    private readonly IExcelReader _excelReader;

    public PessoaController(IPessoaRepository repository, IPessoaValidator validator,
        IExcelReader excelReader)
    {
        _excelReader = excelReader;
        _repository = repository;
        _validator = validator;
        _validator = validator;
    }

    [HttpGet]
    public IActionResult GetPessoas()
    {
        return Ok(_repository.GetPessoas());
    }

    [HttpPost]
    public async Task<IActionResult> Upload(IFormFile? file)
    {
        try
        {
            if (file == null || file.Length == 0)
                return BadRequest("Por favor, faça upload de um arquivo Excel válido.");

            IEnumerable<IPessoa> pessoas;

            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                pessoas = _excelReader.ExtractPessoasFromExcel(stream);
            }

            var pessoasValidas = pessoas.Where(p => _validator.Validate(p)).ToList();

            return Ok(_repository.AddPessoas(pessoasValidas));
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpDelete]
    public IActionResult DeletePessoas()
    {
        _repository.DeleteAll();
        return Ok();
    }
}