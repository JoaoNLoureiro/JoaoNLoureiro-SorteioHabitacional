﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Models;
using OfficeOpenXml;
using System.Globalization;

namespace JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Services;

public class ExcelReader : IExcelReader
{
    public ExcelReader()
    {
        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
    }

    public IEnumerable<IPessoa> ExtractPessoasFromExcel(Stream stream)
    {
        var pessoas = new List<IPessoa>();

        using var package = new ExcelPackage(stream);

        var worksheet = package.Workbook.Worksheets[0];

        for (int row = 2; row <= worksheet.Dimension.Rows; row++)
        {
            var dadosPessoa = worksheet.Cells[row, 1]?.Value?.ToString().Split(',');

            var dataNascimentoValida = DateTime.TryParseExact(dadosPessoa[2], "d/M/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out var dataNascimento);

            var rendaValida = decimal.TryParse(dadosPessoa?[3], out var renda);

            if (dadosPessoa == null && !dataNascimentoValida || !rendaValida) 
                continue;

            var pessoa = new Pessoa
            {
                Nome = dadosPessoa?[0] ?? string.Empty,
                CPF = dadosPessoa?[1] ?? string.Empty,
                DataNascimento = dataNascimento,
                Renda = renda,
                Cota = dadosPessoa?[4] ?? string.Empty,
                CID = dadosPessoa?[5] ?? string.Empty,
            };

            pessoas.Add(pessoa);
        }

        return pessoas;
    }
}