﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Helpers;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Repositories;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;

namespace JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Services;

public class SorteioService : ISorteioService
{
    private readonly IPessoaRepository _pessoaRepository;
    private readonly IEnumerable<ISorteioStrategy> _sorteioStrategies;
    private readonly IResultadoSorteio _resultadoSorteio;

    public SorteioService(IPessoaRepository pessoaRepository,
        IEnumerable<ISorteioStrategy> sorteioStrategies,
        IResultadoSorteio resultadoSorteio)
    {
        _pessoaRepository = pessoaRepository;
        _sorteioStrategies = sorteioStrategies;
        _resultadoSorteio = resultadoSorteio;
    }

    public IResultadoSorteio RealizarSorteio()
    {
        var listaParticipantes = _pessoaRepository.GetPessoas().ToList();

        foreach (var sorteioStrategy in _sorteioStrategies.OrderBy(s => s.Prioridade))
        {
            var pessoasDaCota = sorteioStrategy.FiltrarPessoas(listaParticipantes).ToList();
            var ganhadoresDaCota = sorteioStrategy.SortearPessoas(pessoasDaCota).ToList();

            if (ganhadoresDaCota.Count <= 0)
                continue;

            foreach (var pessoa in ganhadoresDaCota) 
                listaParticipantes.Remove(pessoa);

            _resultadoSorteio.GanhadoresPorCota.Add(sorteioStrategy.Cota.EnumToString(), ganhadoresDaCota);
        }

        return _resultadoSorteio;
    }
}