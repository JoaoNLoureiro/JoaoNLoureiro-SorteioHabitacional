﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Enums;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;

namespace JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Services.SorteioStrategies;

public class SorteioGeralStrategy : SorteioStrategyBase
{
    public override int QuantidadeGanhadores => 3;
    public override int Prioridade => int.MaxValue;
    public override CotaEnum Cota => CotaEnum.Geral;

    public override IEnumerable<IPessoa> FiltrarPessoas(IEnumerable<IPessoa> pessoas)
    {
        return pessoas;
    }
}