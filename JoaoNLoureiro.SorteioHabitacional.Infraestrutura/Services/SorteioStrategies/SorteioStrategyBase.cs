﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Enums;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;

namespace JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Services.SorteioStrategies;

public abstract class SorteioStrategyBase : ISorteioStrategy
{
    public abstract int QuantidadeGanhadores { get; }
    public abstract int Prioridade { get; }
    public abstract CotaEnum Cota { get; }
    public abstract IEnumerable<IPessoa> FiltrarPessoas(IEnumerable<IPessoa> pessoas);

    public IEnumerable<IPessoa> SortearPessoas(IEnumerable<IPessoa> pessoas)
    {
        if (!pessoas.Any())
            return Enumerable.Empty<IPessoa>();

        var rand = new Random();
        return pessoas.OrderBy(_ => rand.Next()).Take(QuantidadeGanhadores);
    }
}