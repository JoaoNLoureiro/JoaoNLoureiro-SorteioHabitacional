﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Enums;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;

namespace JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Services.SorteioStrategies;

public class SorteioDeficienteStrategy : SorteioStrategyBase
{
    public override int QuantidadeGanhadores => 1;
    public override int Prioridade => 0;
    public override CotaEnum Cota => CotaEnum.DeficienteFisico;

    public override IEnumerable<IPessoa> FiltrarPessoas(IEnumerable<IPessoa> pessoas)
    {
        return pessoas.Where(p => p.EDeficienteFisico());
    }
}