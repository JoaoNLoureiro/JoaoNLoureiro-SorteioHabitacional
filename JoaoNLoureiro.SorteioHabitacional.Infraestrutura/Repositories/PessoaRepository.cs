﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Repositories;

namespace JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Repositories;

public class PessoaRepository : IPessoaRepository
{
    private List<IPessoa> Pessoas { get; set; } = new();

    public IEnumerable<IPessoa> GetPessoas()
    {
        return Pessoas;
    }

    public IEnumerable<IPessoa> AddPessoa(IPessoa pessoa)
    {
        Pessoas.Add(pessoa);
        return Pessoas;
    }

    public IEnumerable<IPessoa> AddPessoas(IEnumerable<IPessoa> pessoas)
    {
        Pessoas.AddRange(pessoas);
        return Pessoas;
    }

    public void DeleteAll()
    {
        Pessoas.Clear();
    }
}