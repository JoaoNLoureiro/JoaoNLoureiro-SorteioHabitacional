﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Helpers;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Validators;

namespace JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Validators;

public class PessoaValidator : IPessoaValidator
{
    public bool Validate(IPessoa pessoa)
    {
        if (pessoa.Renda is < 1045.00m or > 5225.00m)
            return false;

        if (!pessoa.DataNascimento.HasValue || pessoa.DataNascimento.Value.AddYears(15) > DateTime.Now)
            return false;

        if (!pessoa.CPF.ValidarCPF())
            return false;

        return true;
    }
}