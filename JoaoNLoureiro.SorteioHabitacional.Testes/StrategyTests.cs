using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;
using JoaoNLoureiro.SorteioHabitacional.Dominio.Models;
using JoaoNLoureiro.SorteioHabitacional.Infraestrutura.Services.SorteioStrategies;

namespace JoaoNLoureiro.SorteioHabitacional.Testes
{
    public class StrategyTests
    {
        private readonly SorteioIdosoStrategy _sorteioIdosoStrategy = new();
        private readonly SorteioDeficienteStrategy _sorteioDeficienteStrategy = new();
        private readonly SorteioGeralStrategy _sorteioGeralStrategy = new();

        [Fact]
        public void FiltrarPessoas_DeveRetornarApenasPessoasIdosas()
        {
            var pessoas = new List<IPessoa>
            {
                new Pessoa { Nome = "Jo�o", DataNascimento = DateTime.Now.AddYears(-65) },
                new Pessoa { Nome = "Maria", DataNascimento = DateTime.Now.AddYears(-55) },
                new Pessoa { Nome = "Carlos", DataNascimento = DateTime.Now.AddYears(-70) }
            };

            var resultado = _sorteioIdosoStrategy.FiltrarPessoas(pessoas);

            Assert.All(resultado, pessoa => Assert.True(pessoa.EIdoso()));
            Assert.Equal(2, resultado.Count());
        }

        [Fact]
        public void FiltrarPessoas_DeveRetornarApenasPessoasDeficientesFisicas()
        {
            var pessoas = new List<IPessoa>
            {
                new Pessoa { Nome = "Jo�o", CID = "H80.3" },
                new Pessoa { Nome = "Maria" },
                new Pessoa { Nome = "Carlos", CID = "G40.3" }
            };

            var resultado = _sorteioDeficienteStrategy.FiltrarPessoas(pessoas);

            Assert.All(resultado, pessoa => Assert.True(pessoa.EDeficienteFisico()));
            Assert.Equal(2, resultado.Count());
        }

        [Fact]
        public void FiltrarPessoas_DeveRetornarTodasAsPessoas()
        {
            var pessoas = new List<IPessoa>
            {
                new Pessoa { Nome = "Jo�o" },
                new Pessoa { Nome = "Maria" },
                new Pessoa { Nome = "Carlos" }
            };

            var resultado = _sorteioGeralStrategy.FiltrarPessoas(pessoas);

            Assert.Equal(pessoas, resultado);
        }
    }
}