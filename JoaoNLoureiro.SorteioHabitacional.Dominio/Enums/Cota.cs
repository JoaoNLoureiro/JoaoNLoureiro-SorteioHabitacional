﻿namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Enums;

public enum CotaEnum
{
    Geral,
    Idoso,
    DeficienteFisico
}