﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Enums;

namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Helpers;

public static class CotaConverter
{
    public static CotaEnum StringToEnum(this string cota)
    {
        return cota.ToUpper() switch
        {
            "IDOSO" => CotaEnum.Idoso,
            "DEFICIENTE FÍSICO" => CotaEnum.DeficienteFisico,
            "GERAL" => CotaEnum.Geral,
            _ => throw new ArgumentException($"Cota inválida: {cota}")
        };
    }

    public static string EnumToString(this CotaEnum cota)
    {
        return cota switch
        {
            CotaEnum.Idoso => "IDOSO",
            CotaEnum.DeficienteFisico => "DEFICIENTE FÍSICO",
            CotaEnum.Geral => "GERAL",
            _ => throw new ArgumentException($"Cota inválida: {cota}")
        };
    }
}
