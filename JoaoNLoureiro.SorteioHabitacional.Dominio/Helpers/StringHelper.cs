﻿using System.Text.RegularExpressions;

namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Helpers;

public static class StringHelper
{
    public static bool ValidarCPF(this string cpf) => 
        new Regex(@"^\d{3}\.\d{3}\.\d{3}-\d{2}$").IsMatch(cpf);
}