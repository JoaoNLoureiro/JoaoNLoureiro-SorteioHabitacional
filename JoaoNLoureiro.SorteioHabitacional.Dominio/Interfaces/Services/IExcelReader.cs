﻿namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;

public interface IExcelReader
{
    IEnumerable<IPessoa> ExtractPessoasFromExcel(Stream stream);

}