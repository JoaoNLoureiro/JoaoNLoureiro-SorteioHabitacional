﻿namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;

public interface ISorteioService
{
    IResultadoSorteio RealizarSorteio();
}