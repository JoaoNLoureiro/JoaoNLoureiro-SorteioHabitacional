﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Enums;

namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Services;

public interface ISorteioStrategy
{
    int QuantidadeGanhadores { get; }
    int Prioridade { get; }
    CotaEnum Cota { get; }

    public IEnumerable<IPessoa> FiltrarPessoas(IEnumerable<IPessoa> pessoas);
    IEnumerable<IPessoa> SortearPessoas(IEnumerable<IPessoa> pessoas);

}