﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Enums;

namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;

public interface IResultadoSorteio
{
    public IDictionary<string, IEnumerable<IPessoa>> GanhadoresPorCota { get; set; }
}
