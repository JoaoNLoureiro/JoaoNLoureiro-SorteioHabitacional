﻿namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;

public interface IPessoa
{
    string Nome { get; set; }
    string CPF { get; set; }
    DateTime? DataNascimento { get; set; }
    decimal Renda { get; set; }
    string Cota { get; set; }
    string CID { get; set; }

    public bool EIdoso();
    public bool EDeficienteFisico();
}