﻿namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Validators;

public interface IPessoaValidator
{
    bool Validate(IPessoa pessoa);
}