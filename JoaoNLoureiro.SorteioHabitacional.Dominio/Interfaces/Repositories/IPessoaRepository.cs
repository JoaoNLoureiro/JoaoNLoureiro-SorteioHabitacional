﻿namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces.Repositories;

public interface IPessoaRepository
{
    IEnumerable<IPessoa> GetPessoas();
    IEnumerable<IPessoa> AddPessoa(IPessoa pessoa);
    IEnumerable<IPessoa> AddPessoas(IEnumerable<IPessoa> pessoas);
    void DeleteAll();
}