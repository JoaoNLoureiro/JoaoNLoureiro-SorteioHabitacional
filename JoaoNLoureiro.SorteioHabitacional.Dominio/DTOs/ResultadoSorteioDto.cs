﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;

namespace JoaoNLoureiro.SorteioHabitacional.Dominio.DTOs;

public class ResultadoSorteioDto : IResultadoSorteio
{
    public IDictionary<string, IEnumerable<IPessoa>> GanhadoresPorCota { get; set; } =
        new Dictionary<string, IEnumerable<IPessoa>>();
}