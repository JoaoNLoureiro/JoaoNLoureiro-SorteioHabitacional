﻿using JoaoNLoureiro.SorteioHabitacional.Dominio.Interfaces;

namespace JoaoNLoureiro.SorteioHabitacional.Dominio.Models;

public class Pessoa : IPessoa
{
    public string Nome { get; set; }
    public string CPF { get; set; }
    public DateTime? DataNascimento { get; set; }
    public decimal Renda { get; set; }
    public string Cota { get; set; }
    public string CID { get; set; }


    public bool EIdoso() => DataNascimento.HasValue && DataNascimento.Value.AddYears(60) <= DateTime.Now;

    public bool EDeficienteFisico() => !string.IsNullOrEmpty(CID);
}