# Sorteio de Habitação

Este projeto foi desenvolvido para ajudar a organizar um Sorteio de Habitação. O objetivo é determinar quais pessoas receberão o benefício e conquistarão o sonho da casa própria.

## Estrutura do Sorteio

O Sorteio de Habitação funciona da seguinte maneira: uma lista inicial de 20 pessoas que concorrerá a 5 unidades habitacionais. Dentre essas pessoas, serão realizados três sorteios para encontrar os ganhadores. O sorteio é dividido por tipo de COTA:

- Uma lista para cota IDOSO onde teremos um vencedor.
- Uma lista para cota DEFICIENTE FÍSICO onde teremos um vencedor.
- Uma lista para cota GERAL onde teremos três vencedores.

## Critério de Aceitação

Todas as pessoas precisam ser validadas antes de entrar em uma lista de sorteio. As seguintes regras são para todos:

- Renda entre R$ 1.045,00 a R$ 5.225,00.
- CPF precisa ser válido.
- Idade maior de 15 anos.

Regra especial para cota de IDOSO: a pessoa precisa ter idade superior a 60 anos.
Regra especial para cota de DEFICIENTE FÍSICO: a pessoa precisa ter informado o número do CID.

## Tecnologias Utilizadas

- .NET 8
- Angular 16

## Requisitos

- .NET 8
- Visual Studio 2022
- Node.js 16+
- Angular CLI

## Excutando o projeto

- Clone o repositório no GitHub.
- Abra o projeto no Visual Studio 2022.
- Execute o projeto **SorteioHabitacional.Server**

O projeto angular é iniciado automaticamente.